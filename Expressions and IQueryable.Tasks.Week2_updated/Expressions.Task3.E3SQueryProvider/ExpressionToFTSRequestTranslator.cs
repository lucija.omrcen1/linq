﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Expressions.Task3.E3SQueryProvider
{
    public class ExpressionToFtsRequestTranslator : ExpressionVisitor
    {
        readonly StringBuilder _resultStringBuilder;

        public ExpressionToFtsRequestTranslator()
        {
            _resultStringBuilder = new StringBuilder();
        }

        public string Translate(Expression exp)
        {
            Visit(exp);

            return _resultStringBuilder.ToString();
        }

        #region protected methods

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.DeclaringType == typeof(Queryable)
                && node.Method.Name == "Where")
            {
                var predicate = node.Arguments[1];
                Visit(predicate);

                return node;
            }
            if (node.Method.DeclaringType == typeof(String))
            {
                switch (node.Method.Name)
                {
                    case "Equals":
                        VisitFieldNameConstant(node.Object, node.Arguments[0]);
                        return node;
                    case "StartsWith":
                        VisitFieldNameConstant(node.Object, node.Arguments[0], end: "*");
                        return node;
                    case "EndsWith":
                        VisitFieldNameConstant(node.Object, node.Arguments[0], start: "*");
                        return node;
                    case "Contains":
                        VisitFieldNameConstant(node.Object, node.Arguments[0], start: "*", end: "*");
                        return node;
                }
            }
            return base.VisitMethodCall(node);
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            switch (node.NodeType)
            {
                case ExpressionType.Equal:
                    if (node.Left.NodeType == ExpressionType.MemberAccess && node.Right.NodeType == ExpressionType.Constant)
                    {
                        VisitFieldNameConstant(node.Left, node.Right);
                    }
                    else if (node.Left.NodeType == ExpressionType.Constant && node.Right.NodeType == ExpressionType.MemberAccess)
                    {
                        VisitFieldNameConstant(node.Right, node.Left);
                    }
                    else
                    {
                        throw new NotSupportedException($"Invalid operands for equal expression.");
                    }
                    break;
                case ExpressionType.AndAlso:
                    var leftNode = node.Left;
                    var rightNode = node.Right;
                    VisitAndAlsoExpression(leftNode, rightNode);
                    
                    break;

                default:
                    throw new NotSupportedException($"Operation '{node.NodeType}' is not supported");
            };

            return node;
        }

        private void VisitAndAlsoExpression(Expression first, Expression second)
        {
            Translate(first);
            _resultStringBuilder.Append("&&");
            Translate(second);

        }

        private void VisitFieldNameConstant(Expression first, Expression second, string start = "", string end = "")
        {
            Visit(first);
            _resultStringBuilder.Append("(").Append(start);
            Visit(second);
            _resultStringBuilder.Append(end).Append(")");
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            _resultStringBuilder.Append(node.Member.Name).Append(":");

            return base.VisitMember(node);
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            _resultStringBuilder.Append(node.Value);

            return node;
        }

        #endregion
    }
}
